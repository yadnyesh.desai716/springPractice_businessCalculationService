package org.example;

public interface DataService {
    int[] retrieveData();
}
