package org.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Stream;

@Configuration
@ComponentScan
public class BusinessConfig {

    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(BusinessConfig.class);
        int maxNo = context.getBean(BusinessCalculationService.class).findMax();
        System.out.println("Max number = " + maxNo);
        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
